using System.Collections.Generic;
using System.Data.Common;

namespace CinemaShared.Models {
    public class SnackOrder {
        public int Id { get; set; }
        public HashSet<SnackAmount> SnackAmounts { get; set; }
    }
}