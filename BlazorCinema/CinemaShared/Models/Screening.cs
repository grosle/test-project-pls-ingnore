﻿using System.Collections.Generic;
using System;

namespace CinemaShared.Models
{
    public class Screening
    {
        public int Id { get; set; }
        public Movie Movie { get; set; }
        public Hall Hall { get; set; }
        public DateTime Time { get; set; }
        public ICollection<Reservation> Reservations { get; set; }
    }
}