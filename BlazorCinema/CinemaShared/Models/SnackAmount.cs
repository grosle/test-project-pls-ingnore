namespace CinemaShared.Models {
    public class SnackAmount {
        public int Id { get; set; }
        public Snack Snack { get; set; }
        public int Amount { get; set; }
    }
}