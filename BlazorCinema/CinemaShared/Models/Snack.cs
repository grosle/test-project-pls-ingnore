using System;

namespace CinemaShared.Models
{
    public class Snack
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public decimal Price { get; set; }
    }
}