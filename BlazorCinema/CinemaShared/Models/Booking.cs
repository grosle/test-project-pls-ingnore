﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CinemaShared.Models
{
    public class Booking
    {
        public string Email { get; set; }
        [ForeignKey("FK_Screening")]
        public int ScreeningId { get; set; }
        public List<int> SeatIds { get; set; }
    }
}