﻿using System.Collections.Generic;

namespace CinemaShared.Models
{
    public class Hall
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Number { get; set; }
        public int Rows { get; set; }
    }
}