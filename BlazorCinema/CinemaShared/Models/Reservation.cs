﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace CinemaShared.Models
{
    public class Reservation
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public int SeatId { get; set; }

        [ForeignKey("FK_Screening")]
        public int ScreeningId { get; set; }
        [JsonIgnore]
        public Screening Screening { get; set; }
    }
}