using CinemaShared.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CinemaApi.Services {
    public interface IOrderService {
        SnackOrder CreateOrder(Dictionary<Snack, int> order);
    }
}