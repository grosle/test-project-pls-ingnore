﻿using CinemaShared.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CinemaApi.Services
{
    public static class SavingService
    {
        public static bool SetReservations(Booking booking)
        {
            try
            {
                using var context = new CinemaDbContext();

                foreach (var seatId in booking.SeatIds)
                {
                    context.Reservations.Add(
                            new Reservation
                            {
                                Email = booking.Email,
                                SeatId = seatId,
                                Screening = context.Screenings.Single(s => s.Id == booking.ScreeningId)
                            });
                }
                context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}