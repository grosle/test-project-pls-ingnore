﻿using CinemaShared.Models;
using System.Collections.Generic;
using System.Linq;

namespace CinemaApi.Services
{
    public static class LoadingService
    {
        public static IEnumerable<Reservation> GetReservations(int screeningId)
        {
            using var context = new CinemaDbContext();
            var reservations = context.Reservations.Where(w => w.ScreeningId == screeningId).ToList();
            return reservations;
        }
    }
}