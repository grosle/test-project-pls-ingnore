using CinemaShared.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;

namespace CinemaApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ScreeningsController : ControllerBase
    {
        private CinemaDbContext _context;

        public ScreeningsController(CinemaDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<Screening> GetScreeningsForMovieName([FromQuery] string movieName)
        {
            return _context.Screenings.Include(scr => scr.Movie)
                .Include(scr => scr.Hall)
                .Include(scr => scr.Reservations)
                .Where(screening => screening.Movie.Name == movieName)
                .Where(Screening => Screening.Time > DateTime.Now);
        }
    }
}