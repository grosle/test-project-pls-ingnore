using CinemaShared.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;

namespace CinemaApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SnacksController : ControllerBase
    {
        private CinemaDbContext _context;

        public SnacksController(CinemaDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<Snack> GetSnacks()
        {
            return _context.Snacks.ToList();
        }
    }
}