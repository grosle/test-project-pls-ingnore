﻿using CinemaApi.Services;
using CinemaShared.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CinemaApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CinemaApiController : ControllerBase
    {
        [HttpGet("GetReservations")]
        public IEnumerable<Reservation> GetReservations(int hallId)
        {
            return LoadingService.GetReservations(hallId);
        }
    }
}