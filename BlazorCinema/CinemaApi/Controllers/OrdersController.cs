using CinemaShared.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;
using CinemaApi.Services;
using System.Threading.Tasks;
using System.Net.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc.Infrastructure;

namespace CinemaApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class OrdersController : ControllerBase
    {
        private CinemaDbContext _context;

        public OrdersController(CinemaDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public List<SnackOrder> GetSnackOrders()
        {
            return _context.SnackOrders.Include(so => so.SnackAmounts)
                .ThenInclude(sa => sa.Snack)
                .ToList();
        }

        [HttpPost]
        public ActionResult<SnackOrder> PostOrder([FromBody] Dictionary<int, int> order)
        {
            HashSet<SnackAmount> snackAmounts = new HashSet<SnackAmount>();
            foreach (var item in order)
            {
                var snackAmount = new SnackAmount
                {
                    Snack = _context.Snacks.Where(s => s.Id == item.Key).First(),
                    Amount = item.Value
                };
                snackAmounts.Add(snackAmount);
            }
            var snackOrder = new SnackOrder
            {
                SnackAmounts = snackAmounts
            };
            _context.SnackOrders.Add(snackOrder);
            _context.SaveChanges();
            return CreatedAtAction(nameof(GetSnackOrders), new { id = snackOrder.Id }, snackOrder);
        }
    }
}