﻿using CinemaApi.Services;
using CinemaShared.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace CinemaApi.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class ReservationsController : ControllerBase
    {
        private CinemaDbContext _context;

        public ReservationsController(CinemaDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<Reservation> GetReservations()
        {
            return _context.Reservations;
        }

        [HttpPost]
        public bool BookReservation(Booking booking)
        {
            var IsSuccessful = SavingService.SetReservations(booking);

            if (IsSuccessful)
                return true;

            return false;
        }
    }
}