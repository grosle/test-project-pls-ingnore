﻿using CinemaShared.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace CinemaApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MoviesController : ControllerBase
    {
        private CinemaDbContext _context;

        public MoviesController(CinemaDbContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public ActionResult<Movie> GetMovie([FromRoute] long id)
        {
            var query = _context.Movies.Where(movie => movie.Id == id);
            try
            {
                return Ok(query.First());
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpGet]
        public IEnumerable<Movie> GetMovies()
        {
            return _context.Movies.ToList();
        }
    }
}
