﻿using CinemaShared.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CinemaApi
{
    public class Seed
    {
        public static void Init(IServiceProvider serviceProvider)
        {
            using (var context = new CinemaDbContext(
                serviceProvider.GetRequiredService<DbContextOptions<CinemaDbContext>>()))
            {
                if (context.Movies.Any() == false)
                {
                    context.Movies.AddRange(
                        new Movie
                        {
                            Name = "Aliens",
                            Description = "Weltraumhorror"
                        },
                        new Movie
                        {
                            Name = "Gladiator",
                            Description = "Gladiatorenkämpfe im antiken Rom"
                        });
                }

                context.SaveChanges();

                if (context.Halls.Any() == false)
                {
                    context.Halls.AddRange(
                        new Hall
                        {
                            Id = 1,
                            Name = "Halle 1",
                            Rows = 6,
                            Number = 6,
                        },
                        new Hall
                        {
                            Id = 2,
                            Name = "Halle 2",
                            Rows = 8,
                            Number = 10,
                        });
                }

                context.SaveChanges();

                if (context.Screenings.Any() == false)
                {
                    context.Screenings.AddRange(
                        new Screening
                        {
                            Hall = context.Halls.Single(halls => halls.Id == 1),
                            Movie = context.Movies.Single(s => s.Name == "Aliens"),
                            Time = DateTime.Now.AddHours(5)
                        },
                        new Screening
                        {
                            Hall = context.Halls.Single(halls => halls.Id == 2),
                            Movie = context.Movies.Single(s => s.Name == "Gladiator"),
                            Time = DateTime.Now.AddHours(4)
                        });
                }
                context.SaveChanges();

                if (context.Reservations.Any() == false)
                {
                    context.Reservations.AddRange(
                        new Reservation
                        {
                            Email = "test@keks.com",
                            SeatId = 10,
                            Screening = context.Screenings.Single(s => s.Id == 1)
                        },
                        new Reservation
                        {
                            Email = "test@keks.com",
                            SeatId = 11,
                            Screening = context.Screenings.Single(s => s.Id == 1)
                        },
                        new Reservation
                        {
                            Email = "test@keks.com",
                            SeatId = 12,
                            Screening = context.Screenings.Single(s => s.Id == 2)
                        },
                        new Reservation
                        {
                            Email = "test2@keks.com",
                            SeatId = 40,
                            Screening = context.Screenings.Single(s => s.Id == 1)
                        },
                        new Reservation
                        {
                            Email = "test2@keks.com",
                            SeatId = 41,
                            Screening = context.Screenings.Single(s => s.Id == 2)
                        });
                }
                context.SaveChanges();

                if (context.Snacks.Any() == false) {
                    context.Snacks.AddRange(
                        new Snack {
                            Name = "Kleine Popcorn",
                            Price = 3.99m
                        },
                        new Snack {
                            Name = "Mittlere Popcorn",
                            Price = 5.99m
                        },
                        new Snack {
                            Name = "Große Popcorn",
                            Price = 7.99m
                        },
                        new Snack {
                            Name = "Cola",
                            Price = 3.99m
                        },
                        new Snack {
                            Name = "Sprite",
                            Price = 3.99m
                        },
                        new Snack {
                            Name = "Wasser",
                            Price = 0.99m
                        },
                        new Snack {
                            Name = "Natchos",
                            Price = 4.99m
                        }
                    );
                }
                context.SaveChanges();
            }
        }
    }
}
